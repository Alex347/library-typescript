"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Adherent = void 0;
const Emprunt_1 = require("./Emprunt");
class Adherent {
    constructor(_id, _nom, _prenom) {
        this._id = _id;
        this._nom = _nom;
        this._prenom = _prenom;
        this._emprunt = [];
    }
    get id() {
        return this._id;
    }
    get nom() {
        return this._nom;
    }
    get prenom() {
        return this._prenom;
    }
    description() {
        return `${this._nom.toUpperCase()} ${this._prenom}`;
    }
    emprunte(volume) {
        if (volume == null)
            throw Error("Aucun volume spécifiée lors de l'emprunt (methode Adherent.emprunte)");
        if (!volume.emprunter())
            return false;
        this._emprunt.push(new Emprunt_1.Emprunt(this, volume, new Date()));
        return true;
    }
    emprunteObjet(volume) {
        if (volume == null)
            throw Error("Aucun volume spécifiée lors de l'emprunt (methode Adherent.emprunte2)");
        if (!volume.emprunter())
            return null;
        const emprunt = new Emprunt_1.Emprunt(this, volume, new Date());
        this._emprunt.push(emprunt);
        return emprunt;
    }
    afficherEmprunts() {
        for (let item of this._emprunt) {
            console.log(item.description());
        }
    }
    rechercherEmpruntVolume(volume) {
        if (volume == null) {
            throw Error("Aucun volume spécifiée lors de l'emprunt (methode Adherent.rechercherEmpruntVolume)");
        }
        return this._emprunt.find((val) => {
            val.volume.id === volume.id;
        });
    }
}
exports.Adherent = Adherent;
//# sourceMappingURL=Adherent.js.map