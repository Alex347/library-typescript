"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ouvrage = void 0;
class Ouvrage {
    constructor(_id, _titre, _dateParution) {
        this._id = _id;
        this._titre = _titre;
        this._dateParution = _dateParution;
    }
    description() {
        return `id=${this._id}-${this._titre} paru le ${this._dateParution.toDateString()}`;
    }
    get id() {
        return this._id;
    }
    get titre() {
        return this._titre;
    }
    get dateParution() {
        return this._dateParution;
    }
}
exports.Ouvrage = Ouvrage;
//# sourceMappingURL=Ouvrage.js.map