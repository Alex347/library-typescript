"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Emprunt = void 0;
class Emprunt {
    constructor(_adherent, _volume, _dateEmprunt) {
        this._adherent = _adherent;
        this._volume = _volume;
        this._dateEmprunt = _dateEmprunt;
    }
    get adherent() {
        return this._adherent;
    }
    get volume() {
        return this._volume;
    }
    get dateEmprunt() {
        return this._dateEmprunt;
    }
    description() {
        return `Livre emprunté: ${this._volume.description()} le ${this.dateEmprunt} par ${this._adherent.nom}`;
    }
}
exports.Emprunt = Emprunt;
//# sourceMappingURL=Emprunt.js.map