"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Livre = void 0;
const Volume_1 = require("./Volume");
class Livre extends Volume_1.Volume {
    constructor(id, titre, dateParution, nombreExemplaire, auteur, _isbn) {
        super(id, titre, dateParution, nombreExemplaire, auteur);
        this._isbn = _isbn;
    }
    get isbn() {
        return this._isbn;
    }
    description() {
        return `Livre: ${this._isbn}, ${super.description()}`;
    }
}
exports.Livre = Livre;
//# sourceMappingURL=Livre.js.map