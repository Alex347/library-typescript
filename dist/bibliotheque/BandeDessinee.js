"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BandeDessinee = void 0;
const Volume_1 = require("./Volume");
class BandeDessinee extends Volume_1.Volume {
    constructor(id, titre, dateParution, nombreExemplaire, auteur, _scenariste) {
        super(id, titre, dateParution, nombreExemplaire, auteur);
        this._scenariste = _scenariste;
    }
    get scenariste() {
        return this._scenariste;
    }
    description() {
        return `BD: ${this._scenariste}, ${super.description()}`;
    }
}
exports.BandeDessinee = BandeDessinee;
//# sourceMappingURL=BandeDessinee.js.map