"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bibliotheque = void 0;
class Bibliotheque {
    constructor() {
        this._adherents = [];
        this._ouvrages = [];
    }
    get ouvrages() {
        return this._ouvrages;
    }
    get adherents() {
        return this._adherents;
    }
    //Adherent
    ajouterAdherent(adherent) {
        if (adherent === null) {
            throw Error("Pas d'adhérent ajouté (methode Bibliotheque.ajouterAdherent)");
        }
        if (this.rechercherAdherent(adherent.id)) {
            return false;
        }
        this._adherents.push(adherent);
        return true;
    }
    rechercherAdherent(id) {
        return this._adherents.find(val => {
            val.id == id;
        });
    }
    //Ouvrage
    ajouterOuvrage(ouvrage) {
        if (ouvrage === null) {
            throw Error("Pas d'ouvrage ajouté (methode Bibliotheque.ajouterOuvrage)");
        }
        if (this.rechercherOuvrage(ouvrage.id))
            return false; //only one time
        this._ouvrages.push(ouvrage);
        return true;
    }
    supprimerOuvrage(ouvrage) {
        try {
            const index = this._ouvrages.indexOf(ouvrage);
            if (index > -1) {
                this._ouvrages.splice(index, 1);
            }
            return true;
        }
        catch (_a) {
            return false;
            throw Error("Pas d'ouvrage supprimé (methode Bibliotheque.supprimerOuvrage)");
        }
    }
    rechercherOuvrage(id) {
        return this._ouvrages.find((val) => {
            val.id === id;
        });
    }
    rechercherOuvrageParTitre(titre) {
        return this._ouvrages.find((val) => {
            val.titre === titre;
        });
    }
    emprunte(adherent, volume) {
        if (adherent === null)
            throw Error("L'adhérent est null (method Bibliotheque.emprunt)");
        const emprunt = adherent.emprunteObjet(volume);
        return emprunt !== null;
    }
    afficherEmprunts() {
        for (let item of this._adherents) {
            console.log(`\n ${item.description()} a emprunté:`);
            item.afficherEmprunts();
        }
    }
    afficherEmpruntsAdherent(adherent) {
        if (adherent === null) {
            throw Error("Pas d'ouvrage disponibles (methode Bibliotheque.afficherEmpruntsAdherent)");
        }
        const _adherent = this.rechercherAdherent(adherent.id);
        _adherent === undefined ? console.log("") : _adherent.afficherEmprunts();
    }
    afficherEmprunteursOuvrage(id) {
        const ouvrageRech = this.rechercherOuvrage(id);
        if (ouvrageRech === undefined)
            return; //nothing
        console.log("Liste d'emprunts d'ouvrage: " + ouvrageRech.description());
        for (let item of this._adherents) {
            if (item.rechercherEmpruntVolume(ouvrageRech) !== undefined) {
                console.log(`  ${item.description}`);
            }
        }
    }
}
exports.Bibliotheque = Bibliotheque;
//# sourceMappingURL=Bibliotheque.js.map