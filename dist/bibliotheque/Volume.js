"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Volume = void 0;
const Ouvrage_1 = require("./Ouvrage");
class Volume extends Ouvrage_1.Ouvrage {
    // private _nombreExemplaireDisponible;
    constructor(id, titre, dateParution, _nombreExemplaire, _auteur) {
        super(id, titre, dateParution);
        this._nombreExemplaire = _nombreExemplaire;
        this._auteur = _auteur;
        this._nombreExemplaireEmprunte = 0;
        if (this._nombreExemplaireEmprunte < 0)
            throw Error('Le nombre emprunté doit être positif');
    }
    description() {
        return `${super.description()} écrit par ${this._auteur}, ${this._nombreExemplaireEmprunte} emprunté sur ${this._nombreExemplaire}`;
    }
    emprunter() {
        if (this._nombreExemplaireEmprunte < this._nombreExemplaire) {
            this._nombreExemplaireEmprunte++;
            return true;
        }
        return false;
    }
    get nombreExemplaire() {
        return this._nombreExemplaire;
    }
    get nombreExemplaireEmprunte() {
        return this._nombreExemplaireEmprunte;
    }
    get auteur() {
        return this._auteur;
    }
}
exports.Volume = Volume;
//# sourceMappingURL=Volume.js.map