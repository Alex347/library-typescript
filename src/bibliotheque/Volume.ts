import {Ouvrage} from "./Ouvrage";

export class Volume extends Ouvrage {

    private _nombreExemplaireEmprunte = 0;
    // private _nombreExemplaireDisponible;

    constructor(
        id: number,  titre: string, dateParution: Date,
        private _nombreExemplaire: number,
        private _auteur: string
    ) {
        super(id, titre, dateParution)
        if (this._nombreExemplaireEmprunte < 0 ) throw Error('Le nombre emprunté doit être positif');
    }
    public description(){
        return `${super.description()} écrit par ${this._auteur}, ${this._nombreExemplaireEmprunte} emprunté sur ${this._nombreExemplaire}`
    }

    emprunter ():boolean{
        if(this._nombreExemplaireEmprunte < this._nombreExemplaire) {
            this._nombreExemplaireEmprunte++;
            return true;
        }
        return false;
    }

    get nombreExemplaire(): number {
        return this._nombreExemplaire;
    }

    get nombreExemplaireEmprunte(): number {
        return this._nombreExemplaireEmprunte;
    }

    get auteur(): string {
        return this._auteur;
    }
}