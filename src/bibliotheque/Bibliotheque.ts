import {Adherent} from "./Adherent";
import {Ouvrage} from "./Ouvrage";
import {Volume} from "./Volume";

export class Bibliotheque{
    public _adherents: Adherent [] = [];
    public _ouvrages: Ouvrage [] = [];

    constructor() {}

    get ouvrages(): Ouvrage[]{
        return this._ouvrages
    }

    get adherents(): Adherent[]{
        return this._adherents
    }


    //Adherent
    public ajouterAdherent(adherent: Adherent): boolean{
        if(adherent === null){
            throw Error("Pas d'adhérent ajouté (methode Bibliotheque.ajouterAdherent)")
        }
        if(this.rechercherAdherent(adherent.id)) {
            return false;
        }

        this._adherents.push(adherent);
        return true;

    }
    public rechercherAdherent(id: number): Adherent | undefined{
        return this._adherents.find(val => {
            val.id == id;
        })
    }


    //Ouvrage
    public ajouterOuvrage(ouvrage: Ouvrage): boolean{
        if(ouvrage=== null){
            throw Error("Pas d'ouvrage ajouté (methode Bibliotheque.ajouterOuvrage)")
        }
        if(this.rechercherOuvrage(ouvrage.id)) return false; //only one time

        this._ouvrages.push(ouvrage);
        return true;

    }
    public supprimerOuvrage(ouvrage: Ouvrage): boolean{
       try{
           const index = this._ouvrages.indexOf(ouvrage);
           if (index > -1) {
               this._ouvrages.splice(index, 1);
           }
           return true;
       }
       catch{
           return false;
           throw Error("Pas d'ouvrage supprimé (methode Bibliotheque.supprimerOuvrage)")
        }


    }
    public rechercherOuvrage(id: number): Ouvrage | undefined{
        return this._ouvrages.find((val) => {
            val.id === id
        })
    }
    public rechercherOuvrageParTitre(titre: string): Ouvrage | undefined{
            return this._ouvrages.find((val) => {
            val.titre === titre
        })
    }


    public emprunte(adherent: Adherent, volume: Volume): boolean {
        if (adherent === null) throw Error("L'adhérent est null (method Bibliotheque.emprunt)")

        const emprunt = adherent.emprunteObjet(volume)
        return emprunt !== null;

    }

    public afficherEmprunts(): void{
        for(let item of this._adherents) {
            console.log(`\n ${item.description()} a emprunté:`)
            item.afficherEmprunts();
        }

    }
    public afficherEmpruntsAdherent(adherent: Adherent): void{
        if(adherent === null){
            throw Error("Pas d'ouvrage disponibles (methode Bibliotheque.afficherEmpruntsAdherent)")
        }
        const _adherent:  Adherent = this.rechercherAdherent(adherent.id)

        _adherent === undefined ? console.log("") : _adherent.afficherEmprunts()

    }
    public afficherEmprunteursOuvrage(id: number): void{
        const ouvrageRech:  Ouvrage = this.rechercherOuvrage(id)
        if(ouvrageRech === undefined) return //nothing

        console.log("Liste d'emprunts d'ouvrage: " + ouvrageRech.description())
        for(let item of this._adherents) {
            if(item.rechercherEmpruntVolume(ouvrageRech as Volume) !== undefined){
                console.log(`  ${item.description}`)
            }
        }
    }
}