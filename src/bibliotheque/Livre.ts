import {Volume} from "./Volume";

export class Livre extends Volume{


    constructor(
        id: number,
        titre: string,
        dateParution: Date,
        nombreExemplaire: number,
        auteur: string,
        private _isbn: string
    ) {
        super(id, titre, dateParution, nombreExemplaire, auteur)
    }

    get isbn(): string {
        return this._isbn;
    }
    public description(){
        return `Livre: ${this._isbn}, ${super.description()}`

    }
}