import {Bibliotheque} from "./Bibliotheque";
import {Volume} from "./Volume";
import {Emprunt} from "./Emprunt";

export class Adherent{
    private _emprunt: Emprunt[] = [];
    constructor(
        private _id: number,
        private _nom: string,
        private _prenom: string,
    ) {}

    get id(): number {
        return this._id;
    }

    get nom(): string {
        return this._nom;
    }

    get prenom(): string {
        return this._prenom;
    }


    public description(): string {
        return `${this._nom.toUpperCase()} ${this._prenom}`
    }

    public emprunte(volume: Volume): boolean{
        if (volume == null) throw Error("Aucun volume spécifiée lors de l'emprunt (methode Adherent.emprunte)")

        if (!volume.emprunter()) return false;

        this._emprunt.push(new Emprunt(this, volume, new Date()))
        return true;

    }

    public emprunteObjet(volume: Volume): Emprunt | null{
        if (volume == null)throw Error("Aucun volume spécifiée lors de l'emprunt (methode Adherent.emprunte2)")

        if (!volume.emprunter())return null;

        const emprunt = new Emprunt(this, volume, new Date())
        this._emprunt.push(emprunt)
        return emprunt;

    }

    public afficherEmprunts(): void{
        for (let item of this._emprunt){
            console.log(item.description())
        }

    }

    public rechercherEmpruntVolume(volume: Volume): Emprunt | undefined{
        if (volume == null){
            throw Error("Aucun volume spécifiée lors de l'emprunt (methode Adherent.rechercherEmpruntVolume)")
        }
        return this._emprunt.find((val) => {
            val.volume.id === volume.id
        })

    }
}